import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {Session} from "../../providers/session";

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  constructor(public navCtrl: NavController, public sn:Session) {
    this.sn.getHomeworks().then(
      (response) => {
        if (response) {
          console.log(response);
        }
      },(error) =>{
        console.log(error);
      }
    );
  }

  login() {
    this.navCtrl.push('LoginPage');
  }

  signup() {
    this.navCtrl.push('SignupPage');
  }
}
