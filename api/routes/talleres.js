const express = require('express');
const router = express.Router();
const pg = require('pg');
const path = require('path');
const connectionString = process.env.DATABASE_URL || 'postgres://postgres:admin@localhost:5432/db-taller';

/* GET talleres listing. */
router.get('/', (req, res, next) => {
    const results = [];
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
        // Handle connection errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }
        // SQL Query > Select Data
        const query = client.query('SELECT * FROM talleres;');
        // Stream results back one row at a time
        query.on('row', (row) => {
            results.push(row);
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
            done();
            return res.json(results);
        });
    });
});

/* POST taller listing. */
router.post('/talleres', (req, res, next) => {
    const results = [];
    // Grab data from http request
    const data = {text: req.body.text, complete: false};
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
        // Handle connection errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }
        // SQL Query > Insert Data
        client.query('INSERT INTO items(text, complete) values($1, $2)',
        [data.text, data.complete]);
        // SQL Query > Select Data
        const query = client.query('SELECT * FROM items ORDER BY id ASC');
        // Stream results back one row at a time
        query.on('row', (row) => {
            results.push(row);
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
            done();
        return res.json(results);
        });
    });
});
/* POST taller listing. */
router.post('/api/v1/todos', (req, res, next) => {
    const results = [];
    // Grab data from http request
    const data = {text: req.body.text, complete: false};
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
        // Handle connection errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }
        // SQL Query > Insert Data
        client.query('INSERT INTO items(text, complete) values($1, $2)',
        [data.text, data.complete]);
        // SQL Query > Select Data
        const query = client.query('SELECT * FROM items ORDER BY id ASC');
        // Stream results back one row at a time
        query.on('row', (row) => {
            results.push(row);
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
          done();
          return res.json(results);
        });
    });
});

/* UPDATE talleres listing. */
router.put('/talleres/:idTaller', (req, res, next) => {
    const results = [];
    // Grab data from the URL parameters
    const id = req.params.idTaller;
    // Grab data from http request
    const data = {text: req.body.text, complete: req.body.complete};
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
        // Handle connection errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }
        // SQL Query > Update Data
        client.query('UPDATE talleres SET text=($1), complete=($2) WHERE id=($3)',
        [data.text, data.complete, id]);
        // SQL Query > Select Data
        const query = client.query("SELECT * FROM talleres ORDER BY id ASC");
        // Stream results back one row at a time
        query.on('row', (row) => {
            results.push(row);
        });
        // After all data is returned, close connection and return results
        query.on('end', function() {
            done();
            return res.json(results);
        });
    });
});


/* DELETE taller listing. */
router.delete('/talleres/:idTaller', (req, res, next) => {
    const results = [];
    // Grab data from the URL parameters
    const id = req.params.idTaller;
    // Get a Postgres client from the connection pool
    pg.connect(connectionString, (err, client, done) => {
        // Handle connection errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }
        // SQL Query > Delete Data
        client.query('DELETE FROM talleres WHERE id=($1)', [id]);
        // SQL Query > Select Data
        var query = client.query('SELECT * FROM talleres ORDER BY id ASC');
        // Stream results back one row at a time
        query.on('row', (row) => {
            results.push(row);
        });
        // After all data is returned, close connection and return results
        query.on('end', () => {
            done();
        return res.json(results);
        });
    });
});

module.exports = router;
